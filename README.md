
# Welcome to CaSiAn: A Tool for Calcium Signalling Analysis

CaSiAn is an open source software implemented with java language that provides
a graphical user interface allowing biologists to easily process a large amount
of calcium signals, tune peak detection parameters for each signal, examine
detected peaks/nadirs and access the quantified characteristics of calcium
spikes in form of excel or text files for further analysis.

![CaSian form](images/form.png)

# Features

- Simple to install: Installing CaSiAn requiers Java SE Runtime Environment
  (JRE) 8 or later.
- User-friendly graphical user interface.
- Fast, easy and precisely extracting calcium signal features.
- Support output files *.txt, *.xls, *.dat and *.csv.
- CaSiAn can be installed on the Mac, Linux, Windows XP, Windows 7 and Windows
  Server 64-bit version.

# License and citation

CaSiAn is open source or, more specifically, free software. It is distributed
under the terms of the GNU General Public License (LGPL), which permits use in
proprietary applications. CaSiAn uses
[Jfreechart](http://www.jfree.org/jfreechart/) library for displaying different
plots. Jfreechart is distributed under the terms of the GNU Lesser General
Public Licence (LGPL), which permits use in proprietary applications.

If you use (parts of) this software in your research, please cite our
publication:

> Moein et al. (2018), CaSiAn: a Calcium Signaling Analyzer tool (Bioinformatics, bty281, open access)

# Download

- [The executable (.jar) file of CaSiAn software](data/s1.xls)
- [An example data set for testing CaSiAn](code/CaSiAn_src.zip)
- The [source code of CaSiAn](code/CaSiAn_src.zip), written in NetBeans 8.0.1
- A [alpha version of CaSiAn](code/CaSiAn_alpha.zip) that allows for background
  removal for the 2 fluorescent channels of a ratiometric dye


# How do I get started?

- Read the [Getting started](tutorial.md) tutorial.

# Contact

If you need more information, please contact
[alexander.skupin@uni.lu](mailto:alexander.skupin@uni.lu).
