
## Installation

First you should install CaSiAn. Running CaSiAn requires installing Java
Runtime Environment (JRE) version 1.8.0. We have tested CaSiAn on Mac, Windows
XP, 7 and Server 64- bit version.

- The executable file of CaSiAn (.jar) is available from
  [code/CaSiAn.zip](code/CaSiAn.zip)

After copying the files onto your system, double click on the CaSiAn.jar file
which will serve you the first form of the tool. If the double click command
did not open the software, you can open the software by the command line.
First, go to the directory where the CaSiAn.jar file is located and then type
the command:
```
java -jar CaSiAn.jar.
```

## Main Window

![Main Window](images/tutorial/f1.png)

For opening a series of signals of an experiment you should follow these
instructions:

1. Browse the folder which contains all files of your signals. Please note that CaSiAn can open only files with the .xls, .txt, .csv, .dat extension. The text files should be delimited by ‘,’ or tab separator.
2. Enter the part of the file name which is common between all files that you intend to open.
3. Enter the common string among the column header names that indicate the intensities of the calcium signals.
4. Enter the number of the column that contains the acquisition times of the corresponding calcium signal intensities.
5. If the time column contains the real acquisition times the Time Scaling Factor should be kept as 1 and CaSiAn will use directly the acquisition time values provided. If the time column contains the frame number of the observed intensities instead of the real acquisition times, the user can modify the Time Scaling Factor to the time difference between two consecutive images. Then CaSiAn computes the time of each sample point by multiplying the frame number with the Time Scaling Factor (the time difference between two consecutive images) and use these values for determining the spiking properties.
6. Indicate the number of treatments during the imaging, if there is any one.
7. Load signals into the CaSiAn by clicking on the “open experiment” button.

## Edit Panel

![Edit Panel](images/tutorial/f2.png)

The length of a signal can be edited by the Edit panel, what can be necessary
for transient phases after stimulation. In this panel the user sets the
beginning and the ending of the signal period to be analyzed and then by
pressing the Edit button the signal will be cut.

The Reset button, resets the signal to the initial signal and ignore all
changes. By checking "apply to all" the determined analysis period will be
applied to all signals.

## Peak Detection Panel

![Peak Detection Panel](images/tutorial/f3.png)

Using this panel, the user can find peaks and nadirs within each signal. The
user must identify how much percent of the maximum amplitude of the signal
should be considered as the peak threshold value. The maximal data points
larger than the threshold value are indicated as the signal peaks.

In the Spike width textbox the user can identify at how much percent of the
spike amplitude the spike width should be calculated. Then the user can find
peaks and nadirs of the individual signal by clicking the Find button or for
all signals by checking the “apply to all” check box.

After the first identification of peaks and nadirs of all signals, the user can
remove trends of the time series by choosing the Remove Background option from
the edit menu. For background removal, several background estimators and a
normalization option are provided. For ratiometric dyes we provide a first
alpha version of CaSiAn that allows to remove background for each fluorescent
channel individually. Since this is not extensively tested, we haven’t included
this feature into the standard version of the tool but provide this as a
separated version for the moment.

Some calculated properties of the identified spikes in the individual signal
like the average of the spike widths, the standard deviation of the spike
widths, the average of amplitudes and the standard deviation of amplitudes of
the signal are directly displayed in the Measured Signal Feature panel below.

## The Signal Plot and Interspike Interval over Time

![Signal Plot](images/tutorial/f4.png)

By loading the signals into CaSiAn, all signals are plotted in the tool and the
user can go through the signals by next, previous, first and last buttons.

After finding peaks and nadirs of each signal, CaSiAn plots the Interspike
Intervals (ISIs) over time for each peak. These plots indicates the
distribution of ISIs over time. If a peak or nadir point is wrongly detected by
the software, the user can easily right click on that point and select "Remove
Peak or Nadir" option from appearing popup menu. CaSiAn also enables users to
add peak or nadir manually by doing right click on each point of signal.

## Standard deviation of ISIs (SD) over average of ISIs (Tav)

![Standard deviation](images/tutorial/f5.png)

After detecting peaks and corresponding ISIs of the signals, CaSiAn is able to
plot the standard deviation of ISIs over the average period Tav from the Plot
menu.

This plot often shows a linear relation between SD and Tav. Therefore, CaSiAn
fits a linear line to the data points and displays the slope, the intercept,
the root mean square error of the fit and also the population average of Tav.

The user is able to filter the data points by the number of peaks in each
signal. The signals which have less than the defined number of peaks will be
removed from the data points by clicking on the “Filter signals” button.

For exploring the underlying time course data, the user can click on a data
point in this window and the corresponding time series will be loaded in the
main window.

## Correlation between Signals

![Correlation between Signals](images/tutorial/f6.png)

For a first exploration of correlation of signals, the user can plot the
relation between 2 signals from the Plot menu. There he can chose 2 signals to
be compared and clicking the Plot button plots the intensities relation and
displays the Pearson correlation coefficient and fit quality by RMSE.

From the Analysis menu, the user can also do a first search to find time shifts
tau between signals that optimize correlation between them.

## Saving and importing results and analysis frameworks

Once the experiment is analyzed, the user can export the analysis results from
the File menu. This will export all signal and corresponding identified ISI as
pdf and generate either xls or text files that contain the summarized signal
properties in the AnalysisInfo file and detailed signal characteristics
including individual spike properties and ISIs in the SignalProfile file.

Furthermore, the software is exporting a data1 file that contains the signal
time course. This file can be used to import an experiment from the File menu
once it is exported. CaSiAn will then read the signals and Info files and
display the exported state of the analysis that can subsequently modified.
